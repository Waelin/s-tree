import json
import xmltodict

from testxml import s_tree


class Validator(object):
    def __init__(self):
        self.schema_type = {"string": basestring,
                            "integer": int,
                            "float": float}

    def compare_attribute(self, attribute_value, type_value_schema):
        if type_value_schema == 'string':
            if isinstance(attribute_value, basestring) or isinstance(attribute_value, int) or isinstance(
                    attribute_value, float):
                return True
        if type_value_schema == 'float':
            if isinstance(attribute_value, int) or isinstance(attribute_value, float):
                return True
        if type_value_schema == 'integer':
            if isinstance(attribute_value, int):
                return True
        return False

    def validate(self, tree, schema):
        # print tree
        # print schema
        object_name = tree[0]
        if object_name != schema.get('object_name', None):
            raise Exception('no such object_name' + str(object_name))
        attributes = [x for x in tree if '@' in x[0] or '#' in x[0]]
        schema_attributes = schema.get('attributes', None)
        for attr in attributes:
            if self.compare_attribute(attr[1], schema_attributes.get(attr[0])) == False:
                raise Exception(
                    'wrong value in {}, value type {}'.format(str(attr), str(schema_attributes.get(attr[0]))))
        properties = [x for x in tree[1:] if not '@' in x[0] and not '#' in x[0]]
        schema_properties = schema.get('properties', None)
        if schema.get('type', None) == 'array':
            item = schema.get('items', None)
            item_objs = [x for x in properties if x[0] == item.get('object_name', None)]
            for i in item_objs:
                self.validate(i, item)
            properties = [x for x in properties if not x[0] == item.get('object_name', None)]
        for prop in properties[1:]:
            sp = None
            for x in schema_properties:
                if x.get('object_name', None) == prop[0]:
                    sp = x
            if sp is None:
                raise Exception("No such prop in schema")
            self.validate(prop, sp)


filename = './source/fileEn.xml'
with open(filename, 'r') as f:
    doc = xmltodict.parse(f.read().encode('utf-8'), encoding='utf-8')

with open('./source/valid.json', 'r') as f:
    schema = json.loads(f.read())

print schema

val = Validator()
s_tree_obj = s_tree()
val.validate(s_tree_obj.parse_to_s(doc).tree[0], schema)
