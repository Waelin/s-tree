import json
import xmltodict
from pprint import pprint
from io import open
import codecs
import re
import pyparsing as pp

def to_html(file_in, s_tr, file_out):
    rec = re.compile('%%[^%]*%%')
    with open(file_in, 'r') as f_in:
        text = f_in.read()
    pathes = rec.findall(text)
    spl = rec.split(text)
    res = u''
    for i, pt in enumerate(pathes):
        res += unicode(spl[i])
        try:
            cd = s_tr.cd(pt[2:-2])
            cd = unicode(cd[0][0])
        except ValueError:
            cd = u''
        res += cd
    res = res + unicode(spl[-1])
    with open(file_out, 'w') as f_out:
        text = f_out.write(''.join(res))

def rec_cond(expr, position):
    if isinstance(expr, list):
        if expr[1] == 'AND':
            return rec_cond(expr[0], position) and rec_cond(expr[2], position)
        if expr[1] == 'OR':
            return rec_cond(expr[0], position) or rec_cond(expr[2], position)
        else:
            return eval_cond(expr, position)

def eval_cond(expr, position):
    def val_cond(expr, position):
        if not(expr[0] in "@#"):
            return expr
        for pos in position:
            if isinstance(pos, list) and (pos[0]==expr):
                return str(pos[1])
        raise ValueError("No attribute {}".format(expr))

    signs = [">=", "<=", "!=", ">", "<", "=="]
    var1 = val_cond(expr[0], position)
    var2 = val_cond(expr[2], position)

#     print 'var1{}var2'.format(expr[1]), eval('var1{}var2'.format(expr[1]))
    return eval('var1{}var2'.format(expr[1]))

class s_tree():
    def __init__(self):
        self._tree = None
        self._positions = None
        self._path = None
        self._add_parsing()
        pass
    
    def _add_parsing(self):
        operator = pp.Regex(">=|<=|!=|>|<|==").setName("operator")
        number = pp.Regex(r"[+-]?\d+(:?\.\d*)?(:?[eE][+-]?\d+)?")
        identifier = pp.Word(pp.alphas+"@#", pp.alphanums + "_")
        comparison_term = identifier | number 
        condition = pp.Group(comparison_term + operator + comparison_term)

        self._cond_pars = pp.operatorPrecedence(condition,[
                                    ("AND", 2, pp.opAssoc.LEFT, ),
                                    ("OR", 2, pp.opAssoc.LEFT, ),
                                    ])
    
    @property
    def tree(self):
        return self._tree
    
    @property
    def positions(self):
        return self._positions
    
    @property
    def path(self):
        return '/'.join(self._path)
    
    def _conpath(self, stpath):
        tpath = stpath.split('/')
        if tpath[-1] == '':
            tpath = tpath[:-1]
        path = self._path
        
        if tpath[0]=='':
            path = tpath
        elif tpath[0]=='.':
            path = path + tpath[1:]
        else:
            path = path + tpath
        return path
    
    def _backpath(self, path):
        i = 0
        while True:
            i += 1
            if i >= len(path):
                break
            if path[i] == '..':
                path = path[:i-1] + path[i+1:]
                i -= 2
        return path
    
    def _rec_path(self, positions):
        new_poses = []
        for pos in positions:
            for head in pos:
                if isinstance(head, list):
                    if not (head[0][0] in '@#'):
                        new_poses.append(head[1:])
        if len(new_poses)>0:
            new_poses = new_poses + self._rec_path(new_poses)
        return new_poses
    
    def _goto(self, path):
        positions = [self._tree]
        sc = re.compile('\[[\w@#0-9=><\(\) ]*\]')
        for pt in path[1:]:
            new_poses = []
            if pt == '*':
                positions = self._rec_path(positions)
                continue
            res = sc.search(pt)
            if  res:
                pt, expr = pt[:res.start()], pt[res.start()+1:res.end()-1]
            for pos in positions:
                for head in pos:
                    if head[0] == pt:
                        new_poses.append(head[1:])
            if len(new_poses)==0:
                raise ValueError('This path do not exist ({})'.format(pt))
            positions = new_poses
            if res:
                try:
                    ind = int(expr)
                    if (ind < len(positions)):
                        positions = [positions[ind]]
                except ValueError:
                    new_poses = []
                    expr = self._cond_pars.parseString(expr).asList()[0]
                    for pos in positions:
                        if rec_cond(expr, pos):
                            new_poses.append(pos)
                    positions = new_poses
                        
        self._positions = positions
        self._path = path
        return self
    
    def modify(self, attr, val):
        for pos in self._positions:
            for head in pos:
                if head[0] == attr:
                    head[1] = val
    
    def parse_to_s(self, dtree):
        """ Transform tree from json data to s-tree

        'dtree' : dict
            Loaded json file
        """
        def rec_parse(dtree, pkey=None):
            if isinstance(dtree, dict):
                res = [pkey]
                for key in dtree:
                    kres = rec_parse(dtree[key], key)
                    res = res + kres
                return [res]
            if isinstance(dtree, list):
                res = []
                for dt in dtree:
                    dtres = rec_parse(dt, pkey)
                    res.append(dtres[0])
                return res
            if isinstance(dtree, basestring):
                if ((pkey[0] != '@') and (pkey[0] != '#')):
                    return [[pkey, [u'#text', dtree]] ]
                try: 
                    dtree = int(dtree)
                except ValueError:
                    try:
                        dtree = float(dtree)
                    except ValueError:
                        pass
                return [[pkey, dtree]]
#             return [[pkey, dtree]]
        self._tree = rec_parse(data)[0][1:]
        self._path = ['']
        self._positions = [self._tree]
        return self
    
    def cd(self, stpath):
        path = self._conpath(stpath)
        path = self._backpath(path)  
        
        return self._goto(path).positions

if __name__ == '__main__':
    filename = './source/fileEn.xml'
    with open(filename, 'r', encoding='utf-8') as f:
        read = f.read()
        print read
        doc = xmltodict.parse(read.encode('utf-8'), encoding='utf-8')

    print doc['recipe']['@name']
    print doc

    with codecs.open('./source/output.json', 'w', encoding='utf-8') as wf:
        wf.write(json.dumps(doc))
    s_tree_obj = s_tree()
    print s_tree_obj.parse_to_s(doc).tree